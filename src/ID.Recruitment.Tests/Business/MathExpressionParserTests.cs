﻿using System;
using Xunit;

namespace ID.Recruitment.Business
{
   
    [Collection("Default")]
    public class MathExpressionParserTests
    {
        [Fact]
        public void Parse_SingleAddition()
        {
            // Arrange
            string expression = "2 + 3";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            var result = mathExpressionParser.Parse(expression);
            
            // Assert
            Assert.Equal(5, result);
        }
        
        [Fact]
        public void Parse_SingleMultiplication()
        {
            // Arrange
            string expression = "4 * 4";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            var result = mathExpressionParser.Parse(expression);
            
            // Assert
            Assert.Equal(16, result);
        }

        [Fact]
        public void Parse_CombinedExpressionSimple()
        {
            // Arrange
            string expression = "3 * 5 + 2";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            var result = mathExpressionParser.Parse(expression);
            
            // Assert
            Assert.Equal(17, result);
        }

        [Fact]
        public void Parse_CombinedExpressionOrdered()
        {
            // Arrange
            string expression = "7 + 8 / 2";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            var result = mathExpressionParser.Parse(expression);
            
            // Assert
            Assert.Equal(11, result);
        }

        [Fact]
        public void Parse_CombinedExpressionOrdered2()
        {
            // Arrange
            string expression = "6 / 2 * 3";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            var result = mathExpressionParser.Parse(expression);
            
            // Assert
            Assert.Equal(9, result);
        }

        [Fact]
        public void Parse_Invalid()
        {
            // Arrange
            string expression = "6/ 2 * 3";
            var mathExpressionParser = new MathExpressionParser();

            // Act
            
            // Assert
            Assert.Throws<ApplicationException>(() =>  mathExpressionParser.Parse(expression));
        }
    }
}