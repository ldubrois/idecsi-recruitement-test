﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Xunit;

namespace ID.Recruitment.Business
{
    public class AvatarBusinessTests
    {
        [Fact]
        public void LoadAllProvidersTests()
        {
            // Arrange
            
            // Act 
            var providers = new AvatarBusiness().LoadAllProviders();
            
            // Assert
            Assert.Equal(2, providers.Count);
            
            Assert.IsType<GravatarProvider>(providers[0]);
            Assert.Equal(10, providers[0].Priority);

            Assert.IsType<RobotHashProvider>(providers[1]);
            Assert.Equal(5, providers[1].Priority);
        }

        [Fact]
        public async Task Search_ResultFromGravatar()
        {
            // Arrange
            var email = "ldubrois@idecsi.com";
            
            // Act
            var avatarBusiness = new AvatarBusiness();
            var result = await avatarBusiness.GetAvatar(email);
                 
            // Assert
            Assert.NotNull(result);
        }
        
        [Fact]
        public async Task Search_ResultFromRobotHash()
        {
            // Arrange
            var email = "ldubrois@idecsi.fr";
            
            // Act
            var avatarBusiness = new AvatarBusiness();
            var result = await avatarBusiness.GetAvatar(email);
            
            // Assert
            Assert.NotNull(result);
        }
    }
}