using System;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using ID.Recruitment.Configuration;
using Microsoft.Extensions.Options;
using Xunit;

namespace ID.Recruitment.Business
{
    [Collection("Default")]
    public class UserSecurityBusinessTests
    {
        [Fact]
        public void GeneratePassword_WithAll()
        {
            // Arrange
            var options = new UserSecurityOptions()
            {
                PasswordRequiresCaps = true,
                PasswordRequiresDigits = true,
                PasswordRequiresSymbols = true,
                PasswordMinLength = 5,
                PasswordMaxLength = 10
            };

            
            // Act
            var userSecurityBusiness = new UserSecurityBusiness(Options.Create(options), null);
            var password = userSecurityBusiness.GeneratePassword();

            // Assert
            Assert.True(ContainsChar(password, UserSecurityBusiness.UPPERCASE_LETTERS));
            Assert.True(ContainsChar(password, UserSecurityBusiness.DIGITS));
            Assert.True(ContainsChar(password, UserSecurityBusiness.SYMBOLS));
            Assert.True(password.Length >= options.PasswordMinLength);
            Assert.True(password.Length <= options.PasswordMaxLength);
        }

        [Fact]
        public void GeneratePassword_WithOnlyLowercase()
        {
            // Arrange
            var options = new UserSecurityOptions()
            {
                PasswordRequiresCaps = false,
                PasswordRequiresDigits = false,
                PasswordRequiresSymbols = false,
                PasswordMinLength = 7,
                PasswordMaxLength = 7
            };

            
            // Act
            var userSecurityBusiness = new UserSecurityBusiness(Options.Create(options), null);
            var password = userSecurityBusiness.GeneratePassword();

            // Assert
            Assert.True(password.Length >= options.PasswordMinLength);
            Assert.True(password.Length <= options.PasswordMaxLength);
        }

        [Fact]
        public void GeneratePassword_WithNoSymbols()
        {
            // Arrange
            var options = new UserSecurityOptions()
            {
                PasswordRequiresCaps = true,
                PasswordRequiresDigits = true,
                PasswordRequiresSymbols = false,
                PasswordMinLength = 2,
                PasswordMaxLength = 6
            };

            
            // Act
            var userSecurityBusiness = new UserSecurityBusiness(Options.Create(options), null);
            var password = userSecurityBusiness.GeneratePassword();

            // Assert
            Assert.True(ContainsChar(password, UserSecurityBusiness.UPPERCASE_LETTERS));
            Assert.True(ContainsChar(password, UserSecurityBusiness.DIGITS));
            Assert.True(password.Length >= options.PasswordMinLength);
            Assert.True(password.Length <= options.PasswordMaxLength);
        }

        [Fact]
        public void GeneratePassword_ImpossibleScenario()
        {
            // Arrange
            var options = new UserSecurityOptions()
            {
                PasswordRequiresCaps = true,
                PasswordRequiresDigits = true,
                PasswordRequiresSymbols = true,
                PasswordMinLength = 1,
                PasswordMaxLength = 2
            };
            
            
            // Act
            var userSecurityBusiness = new UserSecurityBusiness(Options.Create(options), null);
            Assert.Throws<ApplicationException>(() => userSecurityBusiness.GeneratePassword());

        }

        private static bool ContainsChar(string value, char[] charList)
        {
            return value.Any(c => charList.Any(c2 => c == c2));
        }
    }
}