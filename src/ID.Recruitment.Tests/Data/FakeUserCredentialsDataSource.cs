﻿using System;
using System.Threading.Tasks;
using ID.Recruitment.Business;
using ID.Recruitment.Models;

namespace ID.Recruitment.Data
{
    public class FakeUserCredentialsDataSource : IUserCredentialsDataSource
    {
        public static readonly Guid USER_1 = Guid.Parse("47E59FA7-610E-40C7-B306-497875BFEB2E");
        public static readonly string USER1_PASSWORD = "SuperPassword#77";

        public static readonly Guid USER_UNKNOWN = Guid.Parse("B396A538-4A27-4567-9890-BA141F27E614");

        public Task<UserCredentials> Load(Guid userId)
        {
            if (userId == USER_1)
            {
                return Task.FromResult(new UserCredentials()
                {
                    UserId = userId,
                    PasswordHash = UserSecurityBusiness.HashPassword(USER1_PASSWORD)
                });
            }

            return Task.FromResult<UserCredentials>(null);
        }

        public Task Save(UserCredentials userCredentials)
        {
            return Task.CompletedTask;
        }
    }
}