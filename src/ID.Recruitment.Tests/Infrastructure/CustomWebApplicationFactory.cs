﻿using ID.Recruitment.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ID.Recruitment.Infrastructure
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Program>
    {
        protected override IHostBuilder CreateHostBuilder()
        {
            var hostBuilder = Host
                .CreateDefaultBuilder()
                .ConfigureLogging(logging =>
                {
                    logging.AddDebug();
                    logging.AddConsole();
                })
                .ConfigureServices(services =>
                {
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<CustomStartup>();
                });
            

            return hostBuilder;
        }

    }
    
    public  class CustomStartup : Startup
    {
        public CustomStartup()
        {
            ConfigureMVC = builder =>
            {
                builder.AddApplicationPart(typeof(Startup).Assembly).AddControllersAsServices();
            };
        }
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            services.AddTransient<IUserCredentialsDataSource, FakeUserCredentialsDataSource>();
        }
    }
}