﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ID.Recruitment.Infrastructure
{
    public class EasterEggMiddlewareTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public EasterEggMiddlewareTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }



        [Fact]
        public async Task ProbeTest()
        {

            // Arrange
            var client = _factory.CreateClient();

            // Act
            List<string> headersList;
            using (var response = await client.GetAsync("/probes/status"))
            {
                response.Headers.TryGetValues(EasterEggMiddleware.HEADER_EASTER, out var headers);
                headersList = headers?.ToList();
            }

            // Assert
            Assert.NotNull(headersList);
            Assert.NotEmpty(headersList);
            Assert.Single(headersList);
            Assert.True(EasterEggMiddleware.CITATIONS.Any(e => e == headersList[0]));
        }
    }
}