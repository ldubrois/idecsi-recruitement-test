﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ID.Recruitment.Data;
using ID.Recruitment.Infrastructure;
using ID.Recruitment.Models;
using Newtonsoft.Json;
using Xunit;

namespace ID.Recruitment.Controllers
{
    public class UsersControllerTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public UsersControllerTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }



        [Fact]
        public async Task ChangePassword_BadOrigin()
        {
            // Arrange
            var client = _factory.CreateClient();
            var userId = FakeUserCredentialsDataSource.USER_1;
            var model = new ChangePasswordModel()
            {
                OldPassword = FakeUserCredentialsDataSource.USER1_PASSWORD,
                NewPassword = "too_simple"
            };

            // Act
            // Assert
            var payload = JsonConvert.SerializeObject(model);
            using (var response = await client.PutAsync($"/users/{userId}/change-password?origin=mobile", new StringContent(payload, Encoding.Default, "application/json")))
            {
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Fact]
        public async Task ChangePassword_UserDoesNotExists()
        {
            // Arrange
            var client = _factory.CreateClient();
            var userId = FakeUserCredentialsDataSource.USER_UNKNOWN;
            var model = new ChangePasswordModel()
            {
                OldPassword = FakeUserCredentialsDataSource.USER1_PASSWORD,
                NewPassword = "too_simple"
            };

            // Act
            // Assert
            var payload = JsonConvert.SerializeObject(model);
            using (var response = await client.PutAsync($"/users/{userId}/change-password?origin=web", new StringContent(payload, Encoding.UTF8, "application/json")))
            {
                Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Fact]
        public async Task ChangePassword_BadNewPassword()
        {
            // Arrange
            var client = _factory.CreateClient();
            var userId = FakeUserCredentialsDataSource.USER_1;
            var model = new ChangePasswordModel()
            {
                OldPassword = FakeUserCredentialsDataSource.USER1_PASSWORD,
                NewPassword = "too_simple"
            };

            // Act
            // Assert
            var payload = JsonConvert.SerializeObject(model);
            using (var response = await client.PutAsync($"/users/{userId}/change-password?origin=web", new StringContent(payload, Encoding.UTF8, "application/json")))
            {
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Fact]
        public async Task ChangePassword_BadOldPassword()
        {
            // Arrange
            var client = _factory.CreateClient();
            var userId = FakeUserCredentialsDataSource.USER_1;
            var model = new ChangePasswordModel()
            {
                OldPassword = "NotThePassword",
                NewPassword = "MyPassword2021!"
            };

            // Act
            // Assert
            var payload = JsonConvert.SerializeObject(model);
            using (var response = await client.PutAsync($"/users/{userId}/change-password?origin=web", new StringContent(payload, Encoding.UTF8, "application/json")))
            {
                Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
            }
        }

        [Fact]
        public async Task ChangePassword_Success()
        {
            // Arrange
            var client = _factory.CreateClient();
            var userId = FakeUserCredentialsDataSource.USER_1;
            var model = new ChangePasswordModel()
            {
                OldPassword = FakeUserCredentialsDataSource.USER1_PASSWORD,
                NewPassword = "MyPassword2021!"
            };

            // Act
            // Assert
            var payload = JsonConvert.SerializeObject(model);
            using (var response = await client.PutAsync($"/users/{userId}/change-password?origin=web", new StringContent(payload, Encoding.UTF8, "application/json")))
            {
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            }
        }
    }
}