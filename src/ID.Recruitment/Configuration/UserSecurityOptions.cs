﻿namespace ID.Recruitment.Configuration
{
    public class UserSecurityOptions
    {
        /// <summary>
        /// Indicates whether the password should contain Caps
        /// </summary>
        /// <returns>
        /// <code>true</code> if the password must contain a caps, <code>false</code> if the password may or may not contain a cap
        /// </returns>
        public bool PasswordRequiresCaps { get; set; } = true;


        /// <summary>
        /// Indicates whether the password should contain digits
        /// </summary>
        /// <returns>
        /// <code>true</code> if the password must contain a digit, <code>false</code> if the password may or may not contain a digit
        /// </returns>
        public bool PasswordRequiresDigits { get; set; } = true;

        /// <summary>
        /// Indicates whether the password should contain symbols
        /// </summary>
        /// <returns>
        /// <code>true</code> if the password must contain a symbol, <code>false</code> if the password may or may not contain a symbol
        /// </returns>
        public bool PasswordRequiresSymbols { get; set; } = true;


        /// <summary>
        /// Indicates the min char count of the password
        /// </summary>
        public int PasswordMinLength { get; set; } = 5;


        /// <summary>
        /// Indicates the max char count of the password
        /// </summary>
        public int PasswordMaxLength { get; set; } = 16;
    }
}