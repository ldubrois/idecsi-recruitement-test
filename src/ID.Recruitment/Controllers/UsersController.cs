﻿using System;
using System.Threading.Tasks;
using ID.Recruitment.Business;
using ID.Recruitment.Models;
using Microsoft.AspNetCore.Mvc;

namespace ID.Recruitment.Controllers
{

    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserSecurityBusiness _userSecurityBusiness;

        public UsersController(IUserSecurityBusiness userSecurityBusiness)
        {
            _userSecurityBusiness = userSecurityBusiness;

        }
        
        /// <summary>
        /// Expected behavior : (no need to provide more details than just the return code)
        /// 204 - if password changed
        /// 400 - if the origin is not 'web' 
        /// 400 - if new password does not meet the password requests
        /// 403 - if previous password is not valid for the given user
        /// 404 - if the user does not exists
        /// </summary>
        public async Task<string> ChangePassword()
        {


            string origin = "";
            Guid userId = Guid.Empty;
            string currentPassword = "";
            string newPassword = "";


            await _userSecurityBusiness.SetPassword(userId, newPassword);
            
            return "Ok !";
        }


        
    }
}
