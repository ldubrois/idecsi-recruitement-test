﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace ID.Recruitment.Controllers
{
    [Route("[controller]")]
    public class ProbesController : Controller
    {
        [HttpGet]
        [Route("[action]")]
        public string Status()
        {
            return "Ok !";
        }
    }
}