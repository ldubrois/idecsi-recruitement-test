﻿using System;
using ID.Recruitment.Business;
using ID.Recruitment.Data;
using ID.Recruitment.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ID.Recruitment
{
    public class Startup
    {

//        ███╗   ███╗ █████╗ ████████╗██╗  ██╗    ██╗  ██╗     █████╗ ██╗     ██╗     
//        ████╗ ████║██╔══██╗╚══██╔══╝██║  ██║    ██║  ██║    ██╔══██╗██║     ██║     
//        ██╔████╔██║███████║   ██║   ███████║    ███████║    ███████║██║     ██║     
//        ██║╚██╔╝██║██╔══██║   ██║   ██╔══██║    ╚════██║    ██╔══██║██║     ██║     
//        ██║ ╚═╝ ██║██║  ██║   ██║   ██║  ██║         ██║    ██║  ██║███████╗███████╗
//        ╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝         ╚═╝    ╚═╝  ╚═╝╚══════╝╚══════╝

        public Action<IMvcBuilder> ConfigureMVC { get; set; } 

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddTransient<IUserSecurityBusiness, UserSecurityBusiness>();

            services.AddTransient<IUserCredentialsDataSource, UserCredentialsDataSource>();

            var mvcBuilder = services.AddControllers(options =>
            {
            });
            
            ConfigureMVC?.Invoke(mvcBuilder);
            
            services.AddSwaggerGen(c =>
            {
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseRouting();

            
            app.UseSwagger(c =>
            {
            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = $"apis/docs/swagger";
            });
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}