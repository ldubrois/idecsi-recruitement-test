﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ID.Recruitment.Infrastructure
{

    /// <summary>
    /// Add a custom header <code>HEADER_EASTER</code> with a randomly peeked citation 
    /// </summary>
    public class EasterEggMiddleware
    {

        public static readonly string[] CITATIONS = new[]
        {
            "En mathematiques, 'evident' est le mot le plus dangereux. - Eric Temple Bell", 
            "En mathematiques, on ne comprend pas les choses, on s’y habitue. - John von Neumann", 
            "Et pourtant elle tourne. - Galilee",
            "La theorie, c'est quand on sait tout et que rien ne fonctionne. La pratique, c'est quand tout fonctionne et que personne ne sait pourquoi. Ici, nous avons reuni theorie et pratique : Rien ne fonctionne... et personne ne sait pourquoi ! - Albert Einstein"
        };

        public const string HEADER_EASTER = "x-easter";

    }
}