﻿using System;

namespace ID.Recruitment.Models
{
    public class UserCredentials
    {
        public Guid UserId { get; set; }
        
        public string PasswordHash { get; set; }
        
        public int FailedCount { get; set; }
    }
}