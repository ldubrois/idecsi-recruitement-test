﻿using System.Runtime.Serialization;

namespace ID.Recruitment.Models
{
    [DataContract]
    public class ChangePasswordModel
    {
        [DataMember]
        public string OldPassword { get; set; }

        [DataMember]
        public string NewPassword { get; set; }

    }
}