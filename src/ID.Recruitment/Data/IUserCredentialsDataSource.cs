﻿using System;
using System.Threading.Tasks;
using ID.Recruitment.Models;

namespace ID.Recruitment.Data
{
    public interface IUserCredentialsDataSource
    {
        Task<UserCredentials> Load(Guid userId);
        
        Task Save(UserCredentials userCredentials);
    }
}