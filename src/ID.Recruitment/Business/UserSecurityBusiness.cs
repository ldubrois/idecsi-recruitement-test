﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using ID.Recruitment.Configuration;
using ID.Recruitment.Data;
using Microsoft.Extensions.Options;

namespace ID.Recruitment.Business
{

    public class UserSecurityBusiness : IUserSecurityBusiness
    {

        public static readonly char[] LOWERCASE_LETTERS = new char[]
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
        };

        public static readonly char[] UPPERCASE_LETTERS = new char[]
        {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };

        public static readonly char[] DIGITS = new char[]
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };

        public static readonly char[] SYMBOLS = new char[]
        {
            '#', '&', '{', '(', '[', '-', '\\', '_', ']', ')', '@', '}', '=', '+', '-', '*', '/', '%', '$', '£', '!', ':', ';', '?', '.'
        };

        private readonly IOptions<UserSecurityOptions> _options;
        private readonly IUserCredentialsDataSource _dataSource;

        public UserSecurityBusiness(IOptions<UserSecurityOptions> options, IUserCredentialsDataSource dataSource)
        {
            _options = options;
            _dataSource = dataSource;
        }

        public async Task<bool> Exists(Guid userId)
        {
            return (await _dataSource.Load(userId)) != null;
        }
        
        public async Task<bool> CheckPassword(Guid userId, string password)
        {
            var userCredentials = await _dataSource.Load(userId);
            if (userCredentials == null)
            {
                throw new ApplicationException("User does not exists");
            }
            var passwordHashed = HashPassword(password);
            return userCredentials.PasswordHash == passwordHashed;
        }

        public async Task SetPassword(Guid userId, string password)
        {
            var userCredentials = await _dataSource.Load(userId);
            if (userCredentials == null)
            {
                throw new ApplicationException("User does not exists");
            }
            if (!IsPasswordValid(password))
            {
                throw new ApplicationException("Password does not meet requirements");
            }
            
            userCredentials.PasswordHash = HashPassword(password);

            await _dataSource.Save(userCredentials);
        }

        /// <summary>
        /// Generates a password meeting the requirements indicated by the options
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public string GeneratePassword()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ensure the password provided respects the requirements defined in the Options
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool IsPasswordValid(string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///Hash of the password
        /// </summary>
        /// <returns></returns>
        public static string HashPassword(string password)
        {
            StringBuilder sb = new StringBuilder();
            using (HashAlgorithm algorithm = SHA256.Create())
            {
                var bytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(password));
                foreach (byte b in bytes)
                    sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}