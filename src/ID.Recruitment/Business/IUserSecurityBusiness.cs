﻿using System;
using System.Threading.Tasks;

namespace ID.Recruitment.Business
{
    public interface IUserSecurityBusiness
    {
        Task<bool> Exists(Guid userId);

        bool IsPasswordValid(string password);

        Task<bool> CheckPassword(Guid userId, string password);

        Task SetPassword(Guid userId, string password);

        string GeneratePassword();
    }
}