﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ID.Recruitment.Business
{
    public interface IAvatarBusiness
    {
        Task<byte[]> GetAvatar(string email);

        List<IAvatarProvider> LoadAllProviders();
    }
}