﻿namespace ID.Recruitment.Business
{
    public interface IMathExpressionParser
    {
        int Parse(string mathExpression);
    }
}