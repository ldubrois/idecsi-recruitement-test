﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ID.Recruitment.Business
{

    public class AvatarBusiness : IAvatarBusiness
    {
        public async Task<byte[]> GetAvatar(string email)
        {
            var providers = LoadAllProviders();

            foreach (var avatarProvider in providers)
            {
                var content = await avatarProvider.Search(email);

                if (content != null)
                {
                    return content;
                }
            }

            return null;
        }

        public List<IAvatarProvider> LoadAllProviders()
        {
            throw new NotImplementedException();
        }
    }
}