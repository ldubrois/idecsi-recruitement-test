﻿using System.Threading.Tasks;

namespace ID.Recruitment.Business
{
    public interface IAvatarProvider
    {
        /// <summary>
        /// The priority of the provider 0 (low), 100 (high)
        /// </summary>
        int Priority { get; }
        
        /// <summary>
        /// Search for an avatar image
        /// </summary>
        /// <param name="email"></param>
        /// <returns><code>NULL</code> if the provider does not know the user, otherwise a picture</returns>
        Task<byte[]> Search(string email);
    }
}