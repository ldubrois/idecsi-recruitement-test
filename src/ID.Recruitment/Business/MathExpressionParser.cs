﻿using System;

namespace ID.Recruitment.Business
{

    public class MathExpressionParser : IMathExpressionParser
    {
        /*
         * For the test purpose, we do not implement all the aspects of a math expression
         * Only integer are supported (in expression and result)
         * ¤ Each number or operator is always separated by a SPACE
         *      => 6 + 3 * 2 => OK
         *      => 6+3 * 2 => NOK
         * ¤ We do not support parenthesis
         * ¤ Operators supported are only + - * /
         * ¤ Priority is supported (* and /) over ( + and -)
         * ¤ In case of operator priority doubt, perform left to right
         *      => 6 / 2 * 3 => 9
         */
        
        
        public int Parse(string mathExpression)
        {
            throw new NotImplementedException();
        }
    }
}